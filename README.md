# translate

This is a demo app that translate text from one language to another using Open Source LLMs.

It is built using Java Spring Boot and leverage on LangChain4J to interact with the LLMs

# Getting Started

For development, you need to have Java 17 installed as we use Spring Boot v3. Then, clone this repository and run the following command in your terminal:

```sh
# this will download all dependencies and comppile the app
./mvnw clean compile package

# this will start the app for development
./mvnw spring-boot:run
```

# Building and Running the App

To build the app into image,

```sh
./mvnw clean compile package jib:build
```


