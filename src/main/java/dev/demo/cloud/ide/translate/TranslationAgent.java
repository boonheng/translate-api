package dev.demo.cloud.ide.translate;

import dev.langchain4j.service.SystemMessage;
import dev.langchain4j.service.UserMessage;
import dev.langchain4j.service.V;

public interface TranslationAgent {

    @SystemMessage("""
        You are a helpful language expert that translates text from source language to target language.
        You will only output the translated text, and do not provide any explanation or other information. 
        If you do, a kitten will be killed. So obey and save the kitten.
        """)
    @UserMessage(
        """
        Source language: {{sourceLanguage}}\n
        Target language: {{targetLanguage}}\n
        Source Text: {{sourceText}}
        \n\n
        Translated Output: """)
    String translate(
        @V("sourceLanguage") String sourceLanguage, 
        @V("targetLanguage") String targetLanguage, 
        @V("sourceText") String text);

    @SystemMessage(
         """
        You are a helpful language expert that determines the name of the language from a given text.
        You will only output the name of the language in English and nothing else. 
        Do not provide any explanation, label or other information. 
        If you do, a kitten will be killed. So obey and save the kitten.
         """)
    @UserMessage("""
        Given Text: {{sourceText}}\n
        Detected Language: """)
    String detectLanguage(@V("sourceText") String sourceText);

}