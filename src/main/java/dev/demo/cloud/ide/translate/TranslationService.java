package dev.demo.cloud.ide.translate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;

@RequestMapping("/api/v1/language")
@RestController("translation-service")
public class TranslationService {

    @Autowired
    private TranslationAgent translationAgent;

    public TranslationService() {
    }

    @PostMapping(value = "/translate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String translateLanguage(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Translate Request", required = true, content = @io.swagger.v3.oas.annotations.media.Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = LangageTranslateRequestVO.class), examples = @ExampleObject("{ \"text\":\"Hello World!\", \"fromLanguage\":\"English\", \"toLanguage\":\"Chinese\" }"))) @RequestBody LangageTranslateRequestVO request)
            throws Exception {

        String text = request.getText();
        String fromLanguage;
        if (request.getFromLanguage() != null && !request.getFromLanguage().isEmpty()) {
            fromLanguage = request.getFromLanguage();
        } else {
            fromLanguage = translationAgent.detectLanguage(request.getText());
        }
        String toLanguage = request.getToLanguage();

        String translatedText = translationAgent.translate(
                fromLanguage,
                toLanguage,
                text);

        System.out.println(fromLanguage);
        System.out.println(toLanguage);
        System.out.println(translatedText);

        return translatedText;
    }

    @GetMapping("/detect")
    public String detectLanguage(@Schema(example = "今天是工作日，阳光热刮。") String sourceText) {
        return translationAgent.detectLanguage(sourceText);
    }

}
