package dev.demo.cloud.ide.translate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import dev.langchain4j.memory.chat.MessageWindowChatMemory;
import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.service.AiServices;

@Configuration
@ComponentScan
public class GenAIConfiguration {

    @Bean
    TranslationAgent translationAgent(
      ChatLanguageModel chatLanguageModel
    ) {
        return AiServices.builder(TranslationAgent.class)
          .chatLanguageModel(chatLanguageModel)
          .chatMemory(MessageWindowChatMemory.withMaxMessages(10))
          .build();
    }

}
