package dev.demo.cloud.ide.translate;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "LangageTranslateRequest")
final public class LangageTranslateRequestVO implements Serializable {

    private String fromLanguage;
    private String toLanguage;
    private String text;

    public LangageTranslateRequestVO() {
        super();
    }

    public void setFromLanguage(String fromLangCode) {
        this.fromLanguage = fromLangCode;
    }

    public void setToLanguage(String toLangCode) {
        this.toLanguage = toLangCode;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFromLanguage() {
        return fromLanguage;
    }

    public String getToLanguage() {
        return toLanguage;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LangageTranslateRequestVO))
            return false;
        LangageTranslateRequestVO that = (LangageTranslateRequestVO) o;

        return getFromLanguage().equals(that.getFromLanguage()) &&
                getToLanguage().equals(that.getToLanguage()) &&
                getText() != null ? getText().equals(that.getText()) : that.getText() == null;
    }

    @Override
    public String toString() {
        return "LangageTranslateRequestVO{" +
                "fromLanguage='" + fromLanguage + '\'' +
                ", toLanguage='" + toLanguage + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

}
